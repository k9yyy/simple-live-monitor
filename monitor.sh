#!/bin/bash
# Author: inactive-virus <gigagerm@hotmail.com>
# Source: https://gitlab.com/inactive-virus/simple-live-monitor

TARGET="https://www.youtube.com/channel/UCAr7rLi_Wn09G-XfTA07d4g/live"
TAG="kotone"
FLAG=$HOME/${TAG}.flag

[ -n "$1" ] && TARGET="${1}/live"
[ -n "$2" ] && TAG="$2"

date

if [ -z "$STREAMLINK_BIN" ]
  then
    SEARCH_LIST=("$(which streamlink)" "/usr/local/bin/streamlink" "$HOME/.local/bin/streamlink")
    for item in ${SEARCH_LIST[@]}
      do
        [ -x "$item" ] && STREAMLINK_BIN="$item" && break
      done
  if [ -z "$STREAMLINK_BIN" ] 
    then
      echo "无法找到streamlink执行文件，请在脚本前添加 STREAMLINK_BIN=[streamlink路径] " && exit 1
  fi
fi

if [ -e "$FLAG" ]
  then
    echo "正在录制，跳过…" && exit 1
  else
    touch $FLAG
    $STREAMLINK_BIN "$TARGET" best -o "$HOME/${TAG}_$(date '+%y%m%d_%H%M').mp4" --hls-live-restart
    rm $FLAG
fi
